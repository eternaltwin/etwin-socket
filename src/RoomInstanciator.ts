import { APlayer } from "./APlayer";
import { ARealtimeGame } from "./ARealtimeGame";
import { ISocket } from "./socket/ISocket";
import { ResponsePacket } from "./SystemPackets";

export class RoomInstanciator<R extends ARealtimeGame<P>, P extends APlayer>
{
	private readonly gameRooms = new Map<string, R>();
	
	constructor(private readonly RoomInstanciator:(new (opts:any) => R), 
		private readonly PlayerInstanciator:(new (opts:any) => P))
	{

	}

	private getPublicRoom():R|undefined
	{
		for (const [, room] of this.gameRooms) {
			if (room.isPublic && !room.isFilled)
				return (room);
		}
		return (undefined);
	}

	/**
	 * Instanciate a Player object, and bind it to an ISocket.
	 * It also adds the player to a room, or reject the user if the room is fulfilled.
	 * To connect to a room, the user must send an opts parameters with an id field.
	 * If not set, the function will assign the player to a new room.
	 * In all cases, a "response" event will be emitted by the server to tell if there is an error or if the connection was succeessfully established.
	 * 
	 * @param socket:ISocket
	 * @param opts:any
	 * @param playerOpts:any|undefined
	 */
	public instanciatePlayerToRoom = (socket:ISocket, opts:any, playerOpts?:any) =>
	{
		const ID = opts?.id || opts?.roomId;
		const player = new this.PlayerInstanciator(playerOpts);
		let room = this.gameRooms.get(ID);

		player.socket = socket;
		if (ID && room?.isFilled) {
			socket.send<ResponsePacket>("response", {
				status: "KO",
				message: "Room doesn't exist or is already fulfilled."
			});
			socket.destroy();
			return;
		}
		if (!ID) {
			room = this.getPublicRoom();
			if (!room) {
				room = new this.RoomInstanciator(opts);
				this.gameRooms.set(room.ID, room);
			}
		}
		room?.addPlayer(player);
		socket.disconnectCallback = () => room?.removePlayer(player);
		socket.send<ResponsePacket>("response", {
			status: "OK", message: "OK", id: room?.ID
		});
	}
}