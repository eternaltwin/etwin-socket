import http from "http";
import ws from "websocket";
import { IServer, ServerConfiguration, ServerCallback } from "./IServer";
import { WebSocket } from "./../socket/WebSocket";
import { ASocket } from "../socket/ASocket";

export class WebSocketServer implements IServer
{
	private readonly wsServer:ws.server;

	constructor(readonly config:ServerConfiguration, callback:ServerCallback)
	{
		const server = http.createServer((_req, _res) => {});

		server.keepAliveTimeout = ASocket.TIME_BEFORE_CHECKING_TIMEOUT;
		server.listen(config.port, () => {
			console.log(`Web Socket server is listening on port ${config.port}.`);
		});
		this.wsServer = new ws.server({httpServer: server, ...config});
		this.wsServer.on("request", (req:ws.request) => {
			if (!this.isOriginAllowed(req.origin)) {
				req.reject();
				return;
			}
			const socket = req.accept(undefined, req.origin);

			callback(new WebSocket(socket));
			socket.on("error", (err) => {
				console.error(`An error occured on Web Socket server: ${err.stack}.`);
			});
		});
	}

	private isOriginAllowed(_origin:string):boolean
	{
		return (true);
	}
};