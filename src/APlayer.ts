import uniqid from "uniqid";
import { IReceivedPacket, ISendPacket } from "./Packet";
import { ISocket } from "./socket/ISocket";
import { PlayerId } from "./types/PlayerId";

export abstract class APlayer
{
	public readonly ID:PlayerId = uniqid();
	public socket?:ISocket;
	public isSpectator = false;

	public readonly on = <T extends IReceivedPacket>(event:string, callback:(data:T) => void) =>
	{
		this.socket?.on(event, callback);
	}
	public readonly off = (event:string) =>
	{
		this.socket?.remove(event);
	}
	
	/**
	 * This method automatically fill the "senderId" field of the packet which must be sent.
	 */
	public readonly send = <T extends ISendPacket>(event:string, data:T) =>
	{
		data.senderId = this.ID;
		this.socket?.send(event, data);
	}
	public readonly emit = <T extends ISendPacket>(event:string, data:T) => this.send(event, data);
};