import uniqid from "uniqid";
import ws from "websocket";
import { ASocket } from "./ASocket";
import { ISendPacket, IReceivedPacket } from "./../Packet";
import { SocketEvent, SocketId, SocketIp } from "./../types/SocketTypes";

export class WebSocket extends ASocket
{
	readonly ID:SocketId = uniqid();
	readonly IP:SocketIp;

	constructor(private readonly socket:ws.connection)
	{
		super();
		this.IP = this.socket.remoteAddress;
		this.socket.on("message", this.parseRawData);
		this.socket.on("close", this.destroy);
	}

	private parseRawData = (data:ws.IMessage) =>
	{
		const dataAsStr:string|undefined = data.utf8Data;
		let output:IReceivedPacket;
		let callback:Function|undefined;

		if (!dataAsStr || data.type !== "utf8") {
			this.socket.sendUTF("Error: Data type is not a string.");
			return;
		}
		try {
			output = JSON.parse(dataAsStr);
			callback = this.getEvent(output.event || output.eventName);
			if (callback)
				callback(output);
		}
		catch (e) {
			this.socket.sendUTF(`Error: Failed to parse data: ${dataAsStr}.`);
		}
	}

	public send<T extends ISendPacket>(event:SocketEvent, data:T)
	{
		data.event = event;
		data.eventName = event;
		this.socket.sendUTF(JSON.stringify(data));
	}
	public destroy()
	{
		this.disconnectCallback?.();
		this.socket.removeAllListeners();
	}
}